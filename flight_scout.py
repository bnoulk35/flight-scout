import PySimpleGUI as sg
from PIL import Image, ImageTk
import glob
import pyexiv2 as exiv
from fractions import Fraction
from lat_lon_parser import parse
import csv
from pyproj import Transformer


fields = {
    "Xmp.drone-parrot.DroneRollDegree": "Roll",
    "Xmp.drone-parrot.DronePitchDegree": "Pitch",
    "Xmp.drone-parrot.DroneYawDegree": "Yaw",
    "Xmp.exif.GPSLatitude": "Lat",
    "Xmp.exif.GPSLongitude": "Long",
    "Xmp.exif.GPSAltitude": "Altitude (to the WGS84 reference ellipsoid)",
    "Xmp.Camera.GPSXYAccuracy": "XY accuracy",
    "Xmp.Camera.GPSZAccuracy": "Z Accuracy",
    "Xmp.Camera.AboveGroundAltitude": "Above ground altitude",
}


def parse_folder(path):
    images = glob.glob(f'{path}/*.jpg') + glob.glob(f'{path}/*.png')
    return images


def load_image(path, window):
    try:
        image = Image.open(path)
        image.thumbnail((400, 400))
        photo_img = ImageTk.PhotoImage(image)
        with exiv.Image(path, encoding='utf-8') as curr_img:
            Xmp_data = curr_img.read_xmp()
        window["image"].update(data=photo_img)
        for field in fields:
            if field == "Xmp.Camera.GPSXYAccuracy" or field == "Xmp.Camera.GPSZAccuracy" or field == "Xmp.Camera.AboveGroundAltitude" or field == "Xmp.exif.GPSAltitude":
                window[field].update(
                    float(Fraction(Xmp_data.get(field, "no data"))))
            elif field == "Xmp.exif.GPSLatitude" or field == "Xmp.exif.GPSLongitude":
                window[field].update(parse(Xmp_data.get(field, "no data")))
            else:
                window[field].update(Xmp_data.get(field, "no data"))
        return Xmp_data
    except:
        print(f"Unable to open {path} !")


def getxmpdata(path):
    try:
        with exiv.Image(path, encoding='utf-8') as curr_img:
            Xmp_data = curr_img.read_xmp()
        Lat = parse(Xmp_data.get("Xmp.exif.GPSLatitude", "no data"))
        Lon = parse(Xmp_data.get("Xmp.exif.GPSLongitude", "no data"))
        Alt = float(Fraction(Xmp_data.get("Xmp.exif.GPSAltitude", "no data")))
        Altgr = float(Fraction(Xmp_data.get(
            "Xmp.Camera.AboveGroundAltitude", "no data")))
        Pitch = Xmp_data.get("Xmp.drone-parrot.DronePitchDegree", "no data")
        Roll = Xmp_data.get("Xmp.drone-parrot.DroneRollDegree", "no data")
        Yaw = Xmp_data.get("Xmp.drone-parrot.DroneYawDegree", "no data")

    except:
        print("damn it again Xmp is empty")
    return([Lat, Lon, Alt, Altgr, Pitch, Roll, Yaw])


def export_data(corners):
    filename = sg.popup_get_file('Save as', save_as=True, file_types=(
        ("textfile", ".txt"), ("CSV", ".csv")))
    if filename:
        with open(filename, 'w') as f:
            write = csv.writer(f)
            write.writerow(
                ["Lat", "Lon", "Alt", "Altgr", "Pitch", "Roll", "Yaw"])
            data = corners[::-1]
            write.writerows(data)


def read_csv(filename):
    with open(filename, mode='r') as csv_file:  # input
        csv_reader = csv.DictReader(csv_file)
        csv_reader = list(csv_reader)
        return csv_reader


def Flight(n_P, n_L, filename):
    csv_reader = read_csv(filename)
    transformer = Transformer.from_crs(4326, 7789)  # wg84 to itrf2014
    itransformer = Transformer.from_crs(7789, 4326)  # itrf2014 to wg84

    try:
        Yaw = sum(float(image["Yaw"])
                  for image in csv_reader) / len(csv_reader)
        if Yaw < 0:
            Yaw += 360
        Lon = []
        Lat = []
        Altgr = []
        Alt = []
        for i in csv_reader:
            Lon.append(float(i["Lon"]))
            Lat.append(float(i["Lat"]))
            Alt.append(float(i["Alt"]))
            Altgr.append(float(i["Altgr"]))
    except:
        print("file in the wrong format")
    X = []
    Y = []
    Z = []
    Alt[0] = (Alt[0]+Alt[1])/2
    Alt[1] = Alt[0]
    Alt[2] = (Alt[2]+Alt[3])/2
    Alt[3] = Alt[2]
    Altgr[0] = (Altgr[0]+Altgr[1])/2
    Altgr[1] = Altgr[0]
    Altgr[2] = (Altgr[2]+Altgr[3])/2
    Altgr[3] = Altgr[2]
    # print(Alt)
    for i in range(len(Lon)):
        X.append(list(transformer.transform(Lon[i], Lat[i], Alt[i]))[0])
        Y.append(list(transformer.transform(Lon[i], Lat[i], Alt[i]))[1])
        Z.append(list(transformer.transform(Lon[i], Lat[i], Alt[i]))[2])

        # forming the shape of our flight lines array
    lines = [[0 for k in range(n_P)] for h in range(n_L)]
    altgrs = [[0 for k in range(n_P)] for h in range(n_L)]
    for i in range(n_L):
        # populate the first and last point of each lines
        lines[i][0] = [

            ((X[3]-X[0])/(n_L-1))*i+X[0],
            ((Y[3]-Y[0])/(n_L-1))*i+Y[0],
            ((Z[3]-Z[0])/(n_L-1))*i+Z[0]
        ]

        lines[i][n_P-1] = [

            ((X[2]-X[1])/(n_L-1))*i+X[1],
            ((Y[2]-Y[1])/(n_L-1))*i+Y[1],
            ((Z[2]-Z[1])/(n_L-1))*i+Z[1]

        ]
        altgrs[i][0] = ((Altgr[3]-Altgr[0])/(n_L-1))*i+Altgr[0]
        altgrs[i][n_P-1] = ((Altgr[2]-Altgr[1])/(n_L-1))*i+Altgr[1]

        # populating the lines in each point (you get the point..hopefully)
        for j in range(n_P-2):
            f = j+1
            lines[i][f] = [

                ((lines[i][n_P-1][0]-lines[i][0][0])/(n_P-1))*f+lines[i][0][0],
                ((lines[i][n_P-1][1]-lines[i][0][1])/(n_P-1))*f+lines[i][0][1],
                ((lines[i][n_P-1][2]-lines[i][0][2])/(n_P-1))*f+lines[i][0][2]
            ]
            altgrs[i][f] = altgrs[i][0]

    # reverting everything to longitude and latitude and h and add alt from ground

    for i in range(len(lines)):
        for j in range(len(lines[i])):
            lines[i][j] = list(itransformer.transform(
                lines[i][j][0], lines[i][j][1], lines[i][j][2]))
            lines[i][j].append(altgrs[i][j])

    return lines, Yaw

# now the mission planning part


def export_flight(n_L, n_P, data_file, altitude, speed, rotation_speed,delay):
    File = Create_nav_file_array(
        n_L, n_P, data_file, altitude, speed, rotation_speed,delay)
    filename = sg.popup_get_file('Save as', save_as=True, file_types=(
        ("waypoints file", "*.waypoints"),))
    if filename:
        with open(filename, 'w') as f:
            f.write(File[0][0]+"\n")
            write = csv.writer(f, delimiter="\t", lineterminator="\n")
            write.writerows(File[1:])


def Create_nav_file_array(n_L, n_P, data_file, altitude, speed, rotation_speed,delay):
    lines, Yaw = Flight(n_P, n_L, data_file)
    index = 0
    f = 1
    File = [["" for k in range(12)] for i in range(n_L*n_P*2 + 2)]
    File[0][0] = "QGC WPL 110"
    for i in range(len(lines)):
        for j in range(len(lines[i])):
            File[f][0] = index
            File[f][1] = 0
            """ File[f][2] = 0 """
            File[f][3] = 115
            File[f][4] = "{:.8f}".format(Yaw)
            File[f][5] = "{:.8f}".format(float(rotation_speed))
            File[f][6] = "{:.8f}".format(0)
            File[f][7] = "{:.8f}".format(0)
            File[f][8] = "{:.8f}".format(0)
            File[f][9] = "{:.8f}".format(0)
            File[f][10] = "{:.8f}".format(0)
            File[f][11] = 1
            index += 1
            f += 1
            if f == 2:
                File[f][0] = index
                File[f][1] = 0
                File[f][2] = 0
                File[f][3] = 178
                File[f][4] = "{:.8f}".format(0)
                File[f][5] = "{:.8f}".format(float(speed))
                File[f][6] = "{:.8f}".format(0)
                File[f][7] = "{:.8f}".format(0)
                File[f][8] = "{:.8f}".format(0)
                File[f][9] = "{:.8f}".format(0)
                File[f][10] = "{:.8f}".format(0)
                File[f][11] = 1
                index += 1
                f+=1
            File[f][0] = index
            File[f][1] = 0
            File[f][3] = 16
            if (j == 0 or j == len(lines[i])-1):
                File[f][4] = "{:.8f}".format(float(delay))
            else:
                File[f][4] = "{:.8f}".format(0)
            File[f][5] = "{:.8f}".format(0)
            File[f][6] = "{:.8f}".format(0)
            File[f][7] = "{:.8f}".format(0)
            File[f][11] = 1
            if i % 2 != 0:
                if altitude == "Absolute":
                    if f ==3 :
                        File[f-2][2] = 0
                    else:
                        File[f-1][2] = 0
                    File[f][2] = 0
                    File[f][10] = "{:.8f}".format(lines[i][j][2])
                elif altitude == "Relative":
                    if f ==3 :
                        File[f-2][2] = 3
                    else:
                        File[f-1][2] = 3
                    File[f][2] = 3
                    File[f][10] = "{:.8f}".format(lines[i][j][3])
                elif altitude == "Terrain":
                    if f ==3 :
                        File[f-2][2] = 10
                    else:
                        File[f-1][2] = 10
                    File[f][2] = 10
                    File[f][10] = "{:.8f}".format(lines[i][j][3])

                File[f][8] = "{:.8f}".format(lines[i][j][1])
                File[f][9] = "{:.8f}".format(lines[i][j][0])
            else:
                if altitude == "Absolute":
                    if f ==3 :
                        File[f-2][2] = 0
                    else:
                        File[f-1][2] = 0
                    File[f][2] = 0
                    File[f][10] = "{:.8f}".format(lines[i][n_P-j-1][2])
                elif altitude == "Relative":
                    if f ==3 :
                        File[f-2][2] = 3
                    else:
                        File[f-1][2] = 3
                    File[f][2] = 3
                    File[f][10] = "{:.8f}".format(lines[i][n_P-j-1][3])
                elif altitude == "Terrain":
                    if f ==3 :
                        File[f-2][2] = 10
                    else:
                        File[f-1][2] = 10
                    File[f][2] = 10
                    File[f][10] = "{:.8f}".format(lines[i][n_P-j-1][3])
                File[f][8] = "{:.8f}".format(lines[i][n_P-j-1][1])
                File[f][9] = "{:.8f}".format(lines[i][n_P-j-1][0])
            index += 1
            f+=1
    return File


def main():
    data_file = None
    corners = [0, 0, 0, 0]
    sg.theme('Black')
    elements = [
        [sg.Image(key="image")],
        [
            sg.Text("Image Folder"),
            sg.Input(size=(25, 1), enable_events=True, key="file"),
            sg.FolderBrowse(),
        ],
        [
            sg.Button("Prev",),
            sg.Button("Next")
        ],
        [
            sg.Button("Set up-left corner"),
            sg.Button("Set up-right corner"),
            sg.Button("Set down-right corner"),
            sg.Button("Set down-left corner"),
        ],
        [
            sg.Button("Export data")
        ]]
    for field in fields:
        elements += [
            [sg.Text(fields[field], size=(30, 1)),
             sg.Text("", size=(25, 1), key=field)]
        ]
    plan = [
        [sg.Spin([i for i in range(2, 10000)], initial_value=2,
                 key='n_L'), sg.Text('number of flight lines')],
        [sg.Spin([i for i in range(2, 10000)], initial_value=2,
                 k='n_P'), sg.Text('number of points in each line')],
        [sg.Combo(values=('Relative', 'Absolute',  'Terrain'),
                  default_value='Relative', readonly=True, k='Altitude Type')],
        [sg.Text("Speed"), sg.Input(size=(10, 1),
                                    enable_events=True, key="speed")],
        [sg.Text("Rotation speed deg/s"), sg.Input(size=(10, 1),
                                                   enable_events=True, key="rotation_speed")],
        [sg.Text("delay in secs"), sg.Input(size=(10, 1),
                                                   enable_events=True, key="delay")],
        [
            sg.Text("corner csv file"),
            sg.Input(size=(25, 1), enable_events=True, key="cornerfile"),
            sg.FileBrowse(file_types=(("CSV", ".csv"),
                          ("textfile", ".txt")), target="cornerfile")
        ],
        [
            sg.Button("Export flight plan")
        ]]

    layout = [
        [sg.Text('Program that lets you create flight plans from 4 corner images',
                 justification='center', k='-TEXT HEADING-', enable_events=True)]]
    layout += [[sg.TabGroup([[sg.Tab('Image corners', elements, element_justification='c'),
                              sg.Tab('Flight plan', plan, element_justification='c')]], key='-TAB GROUP-')]]
    window = sg.Window("Flight path scout", layout,
                       resizable=True, element_justification='c', auto_size_text=True)
    images = []
    location = 0
    while True:
        event, values = window.read()
        if event == "Exit" or event == sg.WIN_CLOSED:
            break
        if event == "file":
            images = parse_folder(values["file"])
            if images:
                Xmp_data = load_image(images[0], window)
        if event == "Next" and images:
            if location == len(images) - 1:
                location = 0
            else:
                location += 1
            load_image(images[location], window)
        if event == "Prev" and images:
            if location == 0:
                location = len(images) - 1
            else:
                location -= 1
            load_image(images[location], window)
        if event == "Set down-left corner" and images:
            corners[0] = getxmpdata(images[location])
        if event == "Set down-right corner" and images:
            corners[1] = getxmpdata(images[location])
        if event == "Set up-right corner" and images:
            corners[2] = getxmpdata(images[location])
        if event == "Set up-left corner" and images:
            corners[3] = getxmpdata(images[location])
        if event == "Export data" and images:
            export_data(corners)
        if event == "data" and images:
            print(corners)
        if event == "corner csv file":
            data_file = values["cornerfile"]
        if event == "Export flight plan":
            export_flight(values["n_L"], values["n_P"], values["cornerfile"], values["Altitude Type"],
                          values["speed"], values["rotation_speed"],values["delay"])

    window.close()


if __name__ == "__main__":
    main()
