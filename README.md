# Flight scout

A small tool that allows you to generate waypoint files from 4 corner images.

Requirements :
- PySimpleGUI : `pip install PySimpleGUI`
- Pillow : `pip install Pillow`
- Lat_Lon_parser : `pip install lat-lon-parser`
- pyexiv2 : `pip install pyexiv2`
- Pyproj : `pip install pyproj`

